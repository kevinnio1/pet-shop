var Adoption = artifacts.require("Adoption");

var metaAdoption;

contract('Adoption', function (accounts) {

    it("Should adopt", function () {

        return Adoption.deployed().then(function (instance) {
            metaAdoption = instance;
            return metaAdoption.adopt(8, { from: accounts[0], gas: 3000000 });
        }).then(function () {
            return metaAdoption.adopters.call(8);
        }).then(function (adoptersaddress) {
            assert.equal(adoptersaddress, accounts[0], "Should be the same");
        });
    });

    it("Should adopt with async await", async () => {

        const metaAdoptionInstance = await Adoption.deployed();
        await metaAdoptionInstance.adopt(8, { from: accounts[0], gas: 3000000 });
        const adoptersAdress = await metaAdoption.adopters.call(8);
        assert.equal(adoptersAdress, accounts[0], "Should be accounts[0]")

    });

    it("Owner should not be accounts[1] with async await", async () => {

        const metaAdoptionInstance = await Adoption.deployed();
        await metaAdoptionInstance.adopt(8, { from: accounts[0], gas: 3000000 });
        const adoptersAdress = await metaAdoption.adopters.call(8);
        assert.equal(adoptersAdress, accounts[1], "Should be accounts[0]")

    });
});