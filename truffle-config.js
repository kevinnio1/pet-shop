var HDWalletProvider = require("truffle-hdwallet-provider");

module.exports = {
  // See <http://truffleframework.com/docs/advanced/configuration>
  // for more about customizing your Truffle configuration!
  networks: {
    development: {
      host: "127.0.0.1",
      port: 7545,
      network_id: "*" // Match any network id
    },
    rinkeby: {
      provider: function () {
        return new HDWalletProvider("stuff rail fringe benefit cotton track hollow fortune assume hybrid snack brisk", "https://rinkeby.infura.io/v3/a65e91e16f2a4f2b81b3c5f182fa98ad", 1);
      },
      network_id: "*"
    }
  }
};
